package com.example.entity;

import com.j256.ormlite.field.DatabaseField;


public class Preference {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField
	private String desc;
	@DatabaseField
	private boolean active;
	@DatabaseField
	private String action;
	@DatabaseField
	private String context;
	@DatabaseField
	private int minuto;
	@DatabaseField
	private int duracao;
	@DatabaseField
	private boolean horario;
	public Preference(){
		
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMinuto() {
		return minuto;
	}

	public void setMinuto(int minuto) {
		this.minuto = minuto;
	}

	public int getDuracao() {
		return duracao;
	}

	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}

	public boolean isHorario() {
		return horario;
	}

	public void setHorario(boolean horario) {
		this.horario = horario;
	}
	
}
