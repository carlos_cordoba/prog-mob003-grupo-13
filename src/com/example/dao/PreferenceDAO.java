package com.example.dao;

import android.content.Context;

import com.example.entity.Preference;

public class PreferenceDAO extends GenericDAO<Preference>{
	public PreferenceDAO(Context context){
		super(context, Preference.class);
	}
}
