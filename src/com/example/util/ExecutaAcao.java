package com.example.util;

import android.content.Context;
import android.media.AudioManager;
import android.net.wifi.WifiManager;

public class ExecutaAcao {
	private AudioManager audioManager;
	private Context context;
	private WifiManager wifiManager; 

	
	public ExecutaAcao(Context context){
		this.context = context;
		this.audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		this.wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
	}
	
	public void exec(String acao){
		if(acao.equals("Ativar toque")){
			audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
		}
		else if(acao.equals("Silencioso + Vibração")){
			audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
		}
		else if(acao.equals("Desligar toque")){
			audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
		}
		else if(acao.equals("Diminuir brilho")){
			android.provider.Settings.System.putInt(context.getContentResolver(),
			android.provider.Settings.System.SCREEN_BRIGHTNESS, 80);
		}
		else if(acao.equals("Aumentar brilho")){
			android.provider.Settings.System.putInt(context.getContentResolver(),
			android.provider.Settings.System.SCREEN_BRIGHTNESS, 200);
		}
		else if(acao.equals("Ligar wifi")){
			wifiManager.setWifiEnabled(true);
		}
		else if(acao.equals("Desligar wifi")){
			wifiManager.setWifiEnabled(false);
		}
	}
}
