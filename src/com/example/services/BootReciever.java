package com.example.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReciever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent myIntent = new Intent(context, ServiceCall.class);
        Intent wifi = new Intent(context, ServiceWifi.class);
        Intent sms = new Intent(context, ServiceSMS.class);
        Intent baterry = new Intent(context, ServiceLowBaterry.class);
        context.startService(myIntent);
        context.startService(wifi);
        context.startService(baterry);
        context.startService(sms);
        
    }
}