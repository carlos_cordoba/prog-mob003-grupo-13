package com.example.services;

import java.util.Calendar;
import java.util.List;

import com.example.dao.PreferenceDAO;
import com.example.entity.Preference;
import com.example.ifttt_engine.MainActivity;
import com.example.util.ExecutaAcao;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;


public class ServiceCall extends android.app.Service {
	private BroadcastReceiver receiverCall; 
	private PreferenceDAO dao;
		
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(MainActivity.class.getName(), "Serviço iniciado");
        IntentFilter intentf = new IntentFilter("android.intent.action.PHONE_STATE");
        intentf.setPriority(900);
        receiverCall = new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
				// TODO Auto-generated method stub
				String state = intent.getStringExtra("state");
				dao = new PreferenceDAO(context);
				ExecutaAcao acao = new ExecutaAcao(context);
				
				if(state.equals("RINGING")){
					List<Preference> all = dao.getAll();
					Calendar c = Calendar.getInstance();
					int atual = (c.get(Calendar.HOUR_OF_DAY) * 60) + c.get(Calendar.MINUTE);
					for(Preference p : all){
						boolean horario;
						if(p.isHorario()){
							if(atual >= p.getMinuto() && atual <= (p.getMinuto()+p.getDuracao()))
								horario = true;
							else 
								horario = false;
						}
						else
							horario = true;
						if(p.getContext().equals("Chamada recebida") && horario){
							acao.exec(p.getAction());
						}
					}
				}
				
			}
		};
        registerReceiver(receiverCall, intentf);
        
        
        
    }
}