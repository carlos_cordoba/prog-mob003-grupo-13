package com.example.services;

import java.util.Calendar;
import java.util.List;

import com.example.dao.PreferenceDAO;
import com.example.entity.Preference;
import com.example.util.ExecutaAcao;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

public class ServiceLowBaterry extends android.app.Service{
	BroadcastReceiver receiverBaterry;
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onCreate(){
		super.onCreate();
		IntentFilter intentf = new IntentFilter(Intent.ACTION_BATTERY_LOW);
		intentf.setPriority(900);
		receiverBaterry = new BroadcastReceiver(){

			@Override
			public void onReceive(Context context, Intent intent) {
				// TODO Auto-generated method stub
				PreferenceDAO dao = new PreferenceDAO(context);
				ExecutaAcao acao = new ExecutaAcao(context);
				List<Preference> all = dao.getAll();
				Calendar c = Calendar.getInstance();
				int atual = (c.get(Calendar.HOUR_OF_DAY) * 60) + c.get(Calendar.MINUTE);
				for(Preference p : all){
					boolean horario;
					if(p.isHorario()){
						if(atual >= p.getMinuto() && atual <= (p.getMinuto()+p.getDuracao()))
							horario = true;
						else 
							horario = false;
					}
					else
						horario = true;
					if(p.getContext().equals("Bateria fraca") && horario){
						acao.exec(p.getAction());
					}
				}
				
			}
			
		};
		registerReceiver(receiverBaterry, intentf);
		
	}

}
