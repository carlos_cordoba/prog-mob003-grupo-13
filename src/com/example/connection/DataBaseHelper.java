package com.example.connection;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.entity.Preference;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DataBaseHelper<E> extends OrmLiteSqliteOpenHelper {
	public DataBaseHelper(Context context){
		super(context, "banco", null, 3);
	}

	@Override
	public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
		// TODO Auto-generated method stub
		try{
			TableUtils.createTable(arg1, Preference.class);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		try{
			TableUtils.dropTable(arg1, Preference.class , true);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void close(){
		super.close();
	}
	
}
