package com.example.ifttt_engine;

import java.util.Calendar;

import com.example.dao.PreferenceDAO;
import com.example.entity.Preference;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

public class SettingsActivity extends Activity implements OnClickListener{
	private Button salvar;
	private Spinner sp1;
	private Spinner sp2;
	private EditText et;
	private TimePicker timer;
	private EditText duracao;
	private CheckBox horario;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		salvar = (Button) this.findViewById(R.id.salvar1);
		salvar.setOnClickListener(this);
		timer = (TimePicker) this.findViewById(R.id.hora);
		timer.setIs24HourView(true);
		timer.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
		horario = (CheckBox) this.findViewById(R.id.horario);
		horario.setChecked(true);
	}

	@Override 
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		sp1 = (Spinner) this.findViewById(R.id.spinner);
		sp2 = (Spinner) this.findViewById(R.id.spinner2);
		et = (EditText) this.findViewById(R.id.titulo);
		timer = (TimePicker)this.findViewById(R.id.hora);
		duracao = (EditText) this.findViewById(R.id.tempo);
		horario = (CheckBox) this.findViewById(R.id.horario);
		
		Preference p = new Preference();
		p.setDesc(et.getText().toString());
		p.setContext(sp1.getSelectedItem().toString());
		p.setAction(sp2.getSelectedItem().toString());
		p.setMinuto(timer.getCurrentMinute() + (timer.getCurrentHour() * 60));
		boolean aprovado = false;
		if(p.getDesc().equals("")){
			Toast.makeText(this, "Insira um nome", Toast.LENGTH_LONG).show();
			aprovado = false;
		}
		else
			aprovado = true;
		if(duracao.getText().toString().equals("") && horario.isChecked()){
			Toast.makeText(this, "Selecione uma duração, ou desabilite horário", Toast.LENGTH_LONG).show();
			aprovado = false;
		}
		else
			aprovado = true;
		if(!duracao.getText().toString().equals(""))
			p.setDuracao(Integer.parseInt(duracao.getText().toString()));
		else
			p.setDuracao(0);
		p.setHorario(horario.isChecked());
		if(aprovado){
			PreferenceDAO dao = new PreferenceDAO(this);
			dao.insert(p);
			
			Toast.makeText(this, "Salvo", Toast.LENGTH_LONG).show();
			this.finish();
		}
	}
}
