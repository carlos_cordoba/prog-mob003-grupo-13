package com.example.ifttt_engine;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import com.example.dao.PreferenceDAO;
import com.example.entity.Preference;
import com.example.services.ServiceCall;
import com.example.services.ServiceLowBaterry;
import com.example.services.ServiceSMS;
import com.example.services.ServiceWifi;

public class MainActivity extends Activity implements OnClickListener {
	private Button novaConfig;
	private ArrayList<String> a = new ArrayList<String>();
	private ListView lv;
	private PreferenceDAO dao;
	 
	private ArrayAdapter<String> adp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		novaConfig = (Button) this.findViewById(R.id.newconfig);
		novaConfig.setOnClickListener(this);
		dao = new PreferenceDAO(this);
		Intent myIntent = new Intent(this, ServiceCall.class);
		Intent wifi = new Intent(this, ServiceWifi.class);
        Intent sms = new Intent(this, ServiceSMS.class);
        Intent baterry = new Intent(this, ServiceLowBaterry.class);
        startService(myIntent);
        startService(wifi);
        startService(baterry);
        startService(sms);
		
		
		
		
		lv = (ListView) this.findViewById(R.id.configs);
		
		adp = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, a);
		lv.setAdapter(adp);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				//Toast.makeText(getApplicationContext(), "teste", Toast.LENGTH_LONG).show();
				List<Preference> preferences = dao.getAll();
				dao.delete(dao.getById(preferences.get(position).getId()));
				a.remove(position);
				adp.notifyDataSetChanged();
			}
			
		});
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		Intent i = new Intent(this, SettingsActivity.class);
		startActivity(i);
		
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		PreferenceDAO dao = new PreferenceDAO(this);
		List<Preference> listar = dao.getAll();
		if(listar != null){
			a.clear();
			for(Preference p : listar){
				a.add(p.getDesc());
			}
		}
		adp.notifyDataSetChanged();
	}
}
